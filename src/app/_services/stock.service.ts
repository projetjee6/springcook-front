import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {TokenStorageService} from './token-storage.service';
import {Observable} from 'rxjs';
import {ApiResponse} from '../models/response';
import {Ingredient} from '../models/ingredient';
import {Recipe} from '../models/recipe';
import {environment} from "../../environments/environment";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class StockService {
  API_URL = environment.API_URL + '/stock/';
  private user: any;

  constructor(private http: HttpClient, private tokenStorageService: TokenStorageService) {
    this.user = tokenStorageService.getUser();
  }

  getAvailableRecipes(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.API_URL + this.user.id + '/available-recipe', httpOptions);
  }

  getUserIngredients(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.API_URL  + this.user.id + '/ingredient', httpOptions);
  }

  addUserIngredientQuantity(ingredient: string): Observable<ApiResponse> {
    return this.http.patch<ApiResponse>(this.API_URL + this.user.id + '/ingredient/'  + ingredient + '/quantity/1', httpOptions);
  }

  recalculate(recipe: Recipe): Observable<Recipe> {
    return this.http.patch<Recipe>(this.API_URL + this.user.id + '/recipe/' + recipe.id + '/recalculate-stock', httpOptions);
  }

  updateIngredientQuantity(selectedIngredient: Ingredient, selectedQuantity: number) {
    return this.http.patch(this.API_URL + this.user.id + '/ingredient/' + selectedIngredient.id + '/quantity/' + selectedQuantity, httpOptions);
  }

  updateIngredientLimit(selectedIngredient: Ingredient, selectedLimit: number) {
    return this.http.patch(this.API_URL + this.user.id + '/ingredient/' + selectedIngredient.id + '/limit-quantity/' + selectedLimit, httpOptions);
  }

  deleteIngredientFromStock(ingredient_name: string) {
    return this.http.delete(this.API_URL + this.user.id + '/ingredient/' + ingredient_name, httpOptions);
  }

  getIngredientLimit(ingredientId): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.API_URL + this.user.id + '/ingredient/' + ingredientId + '/limit-quantity', httpOptions);
  }

  getIngredientQuantity(ingredientId): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.API_URL + this.user.id + '/ingredient/' + ingredientId + '/quantity', httpOptions);
  }
}
