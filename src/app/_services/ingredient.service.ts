import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {TokenStorageService} from './token-storage.service';
import {Observable} from 'rxjs';
import {Ingredient} from '../models/ingredient';
import {environment} from '../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class IngredientService {
  API_URL = environment.API_URL + '/ingredient/';

  constructor(private http: HttpClient) { }

  getAll(): Observable<Ingredient[]> {
    return this.http.get<Ingredient[]>(this.API_URL, httpOptions);
  }

  getIngredientsByName(name): Observable<Ingredient[]> {
    return this.http.get<Ingredient[]>(this.API_URL + name.name, httpOptions);
  }

  createIngredient(value) {
    console.log(value.name);
    return this.http.post(this.API_URL, {
      baseUnit: `${value.baseUnit.name}`,
      name: `${value.name}`
    }, httpOptions);
  }
}
