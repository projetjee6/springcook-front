import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {TokenStorageService} from './token-storage.service';
import {Observable} from 'rxjs';
import {Recipe} from '../models/recipe';
import {ApiResponse} from '../models/response';
import {environment} from '../../environments/environment';
import {RecipeIngredientDto} from '../models/recipeIngredientDto';
import {IngredientList} from '../models/ingredientList';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  API_URL = environment.API_URL + '/recipe/';

  constructor(private http: HttpClient, private tokenStorageService: TokenStorageService) { }

  getAll(): Observable<Recipe[]> {
    return this.http.get<Recipe[]>(this.API_URL, httpOptions);
  }

  getRecipeByName(name): Observable<Recipe[]> {
    return this.http.get<Recipe[]>(this.API_URL + name.name , httpOptions);
  }

  getRandomRecipes(): Observable<Recipe[]> {
    return this.http.get<Recipe[]>(this.API_URL + 'random-recipe', httpOptions);
  }

  hasIngredient(value): Observable<Recipe[]> {
    console.log(value.nameIngredient.name);
    console.log(value.nameRecipe.name);
    return this.http.get<Recipe[]>(this.API_URL + value.nameRecipe.name + '/ingredient/' + value.nameIngredient.name, httpOptions);
  }

  getIngredientByRecipe(idRecipe): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.API_URL + idRecipe + '/ingredient', httpOptions);
  }

  getPrice(idRecipe): Observable<Recipe[]> {
    return this.http.get<Recipe[]>(this.API_URL + idRecipe + '/price', httpOptions);
  }

  createRecipe(value) {
    return this.http.post(this.API_URL, {
      name: `${value.name}`
    }, httpOptions);
  }

  addIngredientToRecipe(toAdd: RecipeIngredientDto) {
    return this.http.post(this.API_URL + 'ingredient/', toAdd, httpOptions);
  }

  getRecipesByIngredients(ingredientList: IngredientList) {
    console.log('lets filter by ', ingredientList);
    return this.http.post(this.API_URL + 'ingredients', ingredientList, httpOptions);
  }
}
