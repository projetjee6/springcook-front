import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {TokenStorageService} from './token-storage.service';
import {Observable} from 'rxjs';
import {Mealdb} from '../models/mealdb';
import {environment} from "../../environments/environment";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class MealdbService {
  API_URL = environment.API_URL + '/mealdb/';

  constructor(private http: HttpClient) { }

  getMeal(): Observable<Mealdb> {
    return this.http.get<Mealdb>(this.API_URL, httpOptions);
  }
}
