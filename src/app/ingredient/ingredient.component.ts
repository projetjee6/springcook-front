import { Component, OnInit } from '@angular/core';
import {Recipe} from '../models/recipe';
import {Ingredient} from '../models/ingredient';
import {IngredientService} from '../_services/ingredient.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.scss']
})
export class IngredientComponent implements OnInit {
  ingredients: any;
  addIngredientForm: FormGroup;
  searchIngredientForm: FormGroup;
  ingredientSearch: any;
  alertForm: any;
  baseUnit = [
    {name: 'G'},
    {name: 'CL'},
    {name: 'KG'},
    {name: 'ML'},
    {name: 'L'},
    {name: 'CUP'},
    {name: 'TEASPOON'},
    {name: 'TABLESPOON'},
    {name: 'NO_UNIT'},

  ];

  constructor(private ingredientService: IngredientService,  private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.ingredientService.getAll().subscribe((res: Ingredient[]) => {
      this.ingredients = res;
    });

    this.addIngredientForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      baseUnit: ['', [Validators.required]]
    });

    this.searchIngredientForm = this.formBuilder.group({
      name: ['', [Validators.required]]
    });
  }
  onSubmitaddIngredient() {
    if (this.addIngredientForm.invalid) {
      this.alertForm = 1;
      return;
    }
    this.alertForm = 0;
    this.ingredientService.createIngredient(this.addIngredientForm.value)
      .subscribe(
        () => {
          window.location.reload();
        },
        error => {
          console.error(error);
        });
  }

  onSubmitSearchIngredient() {
    if (this.searchIngredientForm.invalid) {
      this.alertForm = 2;
      return;
    }
    this.alertForm = 0;
    this.ingredientService.getIngredientsByName(this.searchIngredientForm.value)
      .subscribe(
        (res) => {
          this.ingredientSearch = res;
        },
        error => {
          this.alertForm = 5;
          console.error(error);
        });
  }
}
