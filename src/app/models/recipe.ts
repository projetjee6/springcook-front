export class Recipe {
  id?: number;
  name: string;
  recipeIngredients: any;
  createdAt: Date;
  updatedAt: Date;
}
