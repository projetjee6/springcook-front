import {Ingredient} from "./ingredient";
import {Recipe} from "./recipe";

export class ApiResponse {
  success?: boolean;
  ingredients?: Ingredient[];
  recipes?: Recipe[];
  used_ingredients?: Ingredient[];
  ingredients_under_limits?: Ingredient[];
  ingredient?: Ingredient;
  quantity?: number;
  unit?: string;
  previous_quantity?: number;
  new_quantity?: number;
  previous_limit?: number;
  new_limit?: number;
  limit?: number;
}
