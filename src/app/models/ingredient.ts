export class Ingredient {
  id: number;
  name: string;
  baseUnit: string;
  recipeIngredients: any;
  stockIngredients: any;
}

export enum BaseUnit {
  G='G',
  CL='CL',
  KG='KG',
  ML='ML',
  L='L',
  CUP='CUP',
  TEASPOON='TEASPOON',
  TABLESPOON='TABLESPOON',
  NO_UNIT='NO_UNIT'
}
