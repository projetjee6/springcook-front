export class RecipeIngredientDto {
  constructor(recipe_name: any, ingredient_name: any, quantity: number) {
    this.recipe_name = recipe_name;
    this.ingredient_name = ingredient_name;
    this.quantity = quantity;
  }

  recipe_name: string;
  ingredient_name: string;
  quantity: number;
}
