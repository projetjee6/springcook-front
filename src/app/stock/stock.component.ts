import { Component, OnInit } from '@angular/core';
import {StockService} from '../_services/stock.service';
import {IngredientService} from '../_services/ingredient.service';
import {Ingredient} from '../models/ingredient';
import {Recipe} from '../models/recipe';

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.scss']
})
export class StockComponent implements OnInit {
  private user: any;
  availablesRecipes = null;
  gettingAvailableRecipes = false;
  addingIngredient = false;
  recalculating = false;
  userIngredients = [];
  allIngredients: Ingredient[];
  ingredientToAdd: any;
  selectedRecipe: Recipe;
  selectedIngredient: Ingredient;
  selectedQuantity: number;
  selectedLimit: number;


  constructor(private stockService: StockService, private ingredientService: IngredientService) { }

  ngOnInit(): void {
    this.getUserIngredients();
    this.ingredientService.getAll().subscribe((res) => {
      this.allIngredients = res;
    });
  }

  getUserIngredients() {
    this.stockService.getUserIngredients().subscribe((res) => {
      this.userIngredients = [];
      res.ingredients.forEach(ingredient => {
        this.stockService.getIngredientQuantity(ingredient.id).subscribe((res) => {
            ingredient.stockIngredients[0].quantity = res.quantity;
            this.stockService.getIngredientLimit(ingredient.id).subscribe((res) => {
                ingredient.stockIngredients[0]['Min quantity'] = res.limit;
                this.userIngredients.push(ingredient);
              },
              error => console.error('Error getting limit for ingredient ' + ingredient.id, error));
          },
          error => console.error('Error getting quantity for ingredient ' + ingredient.id, error));
      });
    });
  }

  getAvailableRecipes() {
    this.gettingAvailableRecipes = true;
    this.recalculating = true;
    this.stockService.getAvailableRecipes().subscribe((res) => {
      this.availablesRecipes = res.recipes;
      this.gettingAvailableRecipes = false;
      this.recalculating = false;
    });
  }

  addUserIngredientQuantity() {
    this.addingIngredient = true;
    this.stockService.addUserIngredientQuantity(this.ingredientToAdd).subscribe((res) => {
      console.log('Ingredient added successfully : ', res);
      this.getUserIngredients();
      this.addingIngredient = false;
    },
      error => {
      console.log('Error while adding ingredient : ', error);
      });
  }

  recalculateStock() {
    this.recalculating = true;
    console.log('lets recalculate stock for recipe :', this.selectedRecipe);
    this.stockService.recalculate(this.selectedRecipe).subscribe((res) => {
      console.log('Stock recalculated');
      this.getUserIngredients();
      this.getAvailableRecipes();
    }, error => {
      console.log('Error : ', error);
    });
  }

  updateIngredientQuantity() {
    console.log('set ' + this.selectedQuantity + ' for ' + this.selectedIngredient.name);
    this.stockService.updateIngredientQuantity(this.selectedIngredient, this.selectedQuantity).subscribe((res) => {
      console.log('Success ', res);
      this.getUserIngredients();
    },
      error => {
      console.log('Error :', error);
      });
  }

  updateIngredientLimit() {
    console.log('set minimum ' + this.selectedLimit + ' for ' + this.selectedIngredient.name);
    this.stockService.updateIngredientLimit(this.selectedIngredient, this.selectedLimit).subscribe((res) => {
        console.log('Success ', res);
        this.getUserIngredients();
      },
      error => {
        console.log('Error :', error);
      });
  }

  deleteIngredientFromStock() {
    this.stockService.deleteIngredientFromStock(this.selectedIngredient.name).subscribe((res) => {
      console.log('Ingredient successfully deleted ', res);
      this.getUserIngredients();
      this.selectedIngredient = null;
    },
      error => {
        console.log('Error ', error);
      });
  }
}
