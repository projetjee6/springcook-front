import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MealdbComponent } from './mealdb.component';

describe('MealdbComponent', () => {
  let component: MealdbComponent;
  let fixture: ComponentFixture<MealdbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MealdbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MealdbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
