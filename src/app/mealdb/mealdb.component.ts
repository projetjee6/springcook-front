import { Component, OnInit } from '@angular/core';
import {Meal, Mealdb} from '../models/mealdb';
import {MealdbService} from '../_services/mealdb.service';

@Component({
  selector: 'app-mealdb',
  templateUrl: './mealdb.component.html',
  styleUrls: ['./mealdb.component.scss'],
})
export class MealdbComponent implements OnInit {
  recipe: Mealdb;
  meal: Meal;
  gettingMeals = false;

  constructor(private MealdbService: MealdbService) { }

  ngOnInit(): void {
  }


  getMeal() {
    console.log('getting meals...');
    this.gettingMeals = true;
    this.MealdbService.getMeal().subscribe((res) => {
      console.log('got meals !', res);
      this.gettingMeals = false;
      this.meal = res.meals[0];
    },
      error => {
        console.error(error);
        this.gettingMeals = false;
      });
  }
}
