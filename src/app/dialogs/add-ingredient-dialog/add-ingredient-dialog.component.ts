import {Component, Inject, OnInit} from '@angular/core';
import {Ingredient} from "../../models/ingredient";
import {RecipeService} from "../../_services/recipe.service";
import {RecipeIngredientDto} from "../../models/recipeIngredientDto";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {IngredientService} from "../../_services/ingredient.service";

export interface DialogData {
  recipe: any;
}

@Component({
  selector: 'app-add-ingredient-dialog',
  templateUrl: './add-ingredient-dialog.component.html',
  styleUrls: ['./add-ingredient-dialog.component.scss']
})
export class AddIngredientDialogComponent implements OnInit {
  ingredientToAdd: any;
  allIngredients: Ingredient[];
  addingIngredient = false;
  quantityToAdd = 1;

  constructor(private recipeService: RecipeService,
              public dialogRef: MatDialogRef<AddIngredientDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData,
              private ingredientService: IngredientService) { }

  ngOnInit(): void {
    this.ingredientService.getAll().subscribe((res) => {
      this.allIngredients = res;
    });
  }

  addIngredientToRecipe() {
    this.addingIngredient = true;
    const toAdd = new RecipeIngredientDto(this.data.recipe.name, this.ingredientToAdd, this.quantityToAdd);
    console.log('toAdd', toAdd);
    this.recipeService.addIngredientToRecipe(toAdd).subscribe((res) => {
        console.log('Ingredient added successfully : ', res);
        this.addingIngredient = false;
      },
      error => {
        console.log('Error while adding ingredient : ', error);
      });
  }
}
