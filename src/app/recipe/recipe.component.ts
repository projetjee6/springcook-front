import { Component, OnInit } from '@angular/core';
import {RecipeService} from '../_services/recipe.service';
import {Recipe} from '../models/recipe';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {AddIngredientDialogComponent} from '../dialogs/add-ingredient-dialog/add-ingredient-dialog.component';
import {Ingredient} from '../models/ingredient';
import {IngredientList} from '../models/ingredientList';
import {IngredientService} from '../_services/ingredient.service';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.scss']
})
export class RecipeComponent implements OnInit {

  recipes: any;
  ingredients: any;
  addRecipeForm: FormGroup;
  searchRecipeForm: FormGroup;
  hasIngredientForm: FormGroup;
  ingredientToFilter: any;
  allIngredients: Ingredient[];
  ingredientsFilter = new IngredientList();
  filteredRecipes = null;
  recipeSearch = null;
  randomRecipe: any;
  hasIngredient: any;
  alertForm: any;

  constructor(private recipeService: RecipeService,
              private formBuilder: FormBuilder,
              public dialog: MatDialog,
              private ingredientService: IngredientService) { }

  ngOnInit() {
    this.getAllRecipe();
    this.addRecipeForm = this.formBuilder.group({
      name: ['', [Validators.required]]
    });
    this.searchRecipeForm = this.formBuilder.group({
      name: ['', [Validators.required]]
    });
    this.ingredientService.getAll().subscribe((res) => {
      this.allIngredients = res;
    });
    this.ingredientsFilter.ingredients = [];
    this.filteredRecipes = [];
    this.hasIngredientForm = this.formBuilder.group({
      nameRecipe: ['', [Validators.required]],
      nameIngredient: ['', [Validators.required]]
    });
  }
  getAllRecipe() {
    this.recipeService.getAll().subscribe((res: Recipe[]) => {
      this.recipes = res;
    });
  }

  getIngredientRecipe(idRecipe) {
    this.ingredients = null;
    this.recipeService.getIngredientByRecipe(idRecipe).subscribe((res) => {
        this.ingredients = res.ingredients;
      },
      error => {
        console.error(error);
      });
  }
  onSubmitAddRecipe() {
    if (this.addRecipeForm.invalid) {
      this.alertForm = 3;
      return;
    }
    this.alertForm = 0;
    this.recipeService.createRecipe(this.addRecipeForm.value)
      .subscribe(
        () => {
          window.location.reload();
        },
        error => {
          console.error(error);
        });
  }

  onSubmitSearchRecipe() {

    if (this.searchRecipeForm.invalid) {
      this.alertForm = 1;
      return;
    }
    this.alertForm = 0;
    this.recipeService.getRecipeByName(this.searchRecipeForm.value)
      .subscribe(
        (res) => {
          this.recipeSearch = res;
        },
        error => {
          this.alertForm = 5;
          console.error(error);
        });
  }

  onSubmitHasIngredient() {

    if (this.hasIngredientForm.invalid) {
      this.alertForm = 2;
      return;
    }
    this.alertForm = 0;
    this.recipeService.hasIngredient(this.hasIngredientForm.value)
      .subscribe(
        (res) => {
          this.hasIngredient = res;
        },
        error => {
          console.error(error);
        });
  }

  addIngredientDialog(recipe) {
    const addIngredientDialog = this.dialog.open(AddIngredientDialogComponent, {
      width: '550px',
      height: '250px',
      data: {recipe}
    });

    // tslint:disable-next-line:no-shadowed-variable
    addIngredientDialog.afterClosed().subscribe(recipe => {
      this.getIngredientRecipe(recipe.id);
    });
  }

  filterRecipesBy() {
    this.ingredientsFilter.ingredients.push(this.ingredientToFilter);
    this.filteredRecipes = null;
    this.recipeService.getRecipesByIngredients(this.ingredientsFilter).subscribe((res) => {
      console.log('got filtered recipes ', res);
      this.filteredRecipes = res;
    });
  }

  resetFilter() {
    this.ingredientsFilter.ingredients = [];
    this.filteredRecipes = [];
  }

  getRandomRecipe() {
    this.recipeService.getRandomRecipes().subscribe((res) => {
      this.randomRecipe = res;
    });
  }
}
